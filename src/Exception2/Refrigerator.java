package Exception2;

import java.util.ArrayList;

public class Refrigerator {
	public int size ;
	public ArrayList<String> things;

	
	public Refrigerator(int size){
		this.size = size;
		things = new ArrayList<String>();
	}
	
	public void put(String stuff) throws FullException{
		if(things.size() >= size){
			throw new FullException();
		}else{
			things.add(stuff);
		}
	}
	
	public String takeOut(String stuff){
		if (things.isEmpty()){
			return "No have" + null;
		}
		for (int i = 0; i < things.size(); i++) {
			if (things.get(i).equals(stuff)){
				things.remove(i);
			}
		}
		return stuff;
	}
	
	public String toString(){
		String list = "";
		list += things;
		return list;
	}
}


