package Exception2;

public class Main {

	public static void main(String[] args) {
		try {
			Refrigerator re = new Refrigerator(6);
			re.put("Chicken");
			re.put("Pork");
			re.put("Orange");
			re.put("Duck");
			re.put("Orange");
			re.put("Lychee");
			re.put("rice");
			
			re.takeOut("Pork");
			re.takeOut("Lychee");
			
			System.out.println(re);
		} catch (FullException e) {
			// TODO: handle exception
			System.out.println("Refrigerator space is Full.");
		}
	}
}
